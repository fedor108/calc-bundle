### Symfony Calc Bundle

## Test
```
./bin/phpunit
```

## Demo
```
./bin/console app:calc "1 + 1 + 2*5 - 6:3"
```

## Use another calc
```
use Fedor108\CalcBundle\Controller\CalcController;

$calc = new CalcController();
$calc->useCalc(new YourCalcClass());
$calc->execute('1+1');
```