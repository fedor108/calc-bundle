<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Fedor108\CalcBundle\Fedor108CalcBundle::class => ['all' => true],
];
