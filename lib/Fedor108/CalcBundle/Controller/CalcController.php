<?php

namespace Fedor108\CalcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Fedor108\CalcBundle\Service\DefaultCalc;
use Fedor108\CalcBundle\Service\CalcInterface;

class CalcController extends AbstractController
{
    private $calc;

    public function __construct()
    {
        $this->setCalc(new DefaultCalc());
    }

    public function setCalc(CalcInterface $calc)
    {
        $this->calc = $calc;
    }

    public function execute($mathString)
    {
        return $this->calc->execute($mathString);
    }
}