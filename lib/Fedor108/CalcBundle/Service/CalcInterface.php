<?php

namespace Fedor108\CalcBundle\Service;

interface CalcInterface
{
    public function execute(string $string): array;
}