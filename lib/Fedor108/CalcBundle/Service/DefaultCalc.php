<?php

namespace Fedor108\CalcBundle\Service;

class DefaultCalc implements CalcInterface
{
    const OPERATORS = ['+', '-', '*', ':'];

    private $errors = [];

    /**
     * @param string $string
     * @return array
     */
    public function execute(string $string): array
    {
        $parsed = $this->parse($string);

        $result = $this->calc($parsed);

        $errors = $this->errors;

        return compact('result', 'errors', 'parsed', 'string');
    }

    /**
     * @param string $string
     * @return array|string
     */
    public function parse(string $string)
    {
        $parsed = [];

        foreach (self::OPERATORS as $operator) {
            if (false === strpos($string, $operator)) {
                continue;
            }

            $parsed[$operator] = array_map('trim', explode($operator, $string));

            foreach ($parsed[$operator] as &$item) {
                $item = $this->parse($item);
            }

            break;
        }

        return empty($parsed) ? $string : $parsed;
    }

    /**
     * @param $parsed
     * @return float|int|mixed|null
     */
    public function calc($parsed)
    {
        if (!is_array($parsed)) {
            return $parsed;
        }

        $operator = array_key_first($parsed);

        $result = reset($parsed[$operator]);

        foreach ($parsed[$operator] as $i => $item) {
            $value = $this->calc($item);

            if (0 == $i) {
                $result = $value;
            } else {
                $result = $this->useOperator($result, $value, $operator);
            }
        }

        return $result;
    }

    /**
     * @param $x
     * @param $y
     * @param $operator
     * @return float|int|null
     */
    public function useOperator($x, $y, $operator)
    {
        switch ($operator) {
            case '*':
                return $x * $y;
            case ':':
                if (empty($y)) {
                    $this->errors[] = 'Division by zero';
                    return null;
                }
                return $x / $y;
            case '+':
                return $x + $y;
            case '-':
                return $x - $y;
            default:
                $this->errors[] = 'Unknown operator: ' . $operator;
                return null;
        }
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}