<?php

namespace Fedor108\CalcBundle\Tests;

use Fedor108\CalcBundle\Service\DefaultCalc;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class DefaultCalcTest extends TestCase
{
    public function testParse()
    {
        $calc = new DefaultCalc();

        $parsed = $calc->parse('1+2');

        $this->assertEquals(['+' => ['1', '2']], $parsed);
    }

    public function testUseOperator()
    {
        $calc = new DefaultCalc();

        $result = $calc->useOperator('1', '2', '+');

        $this->assertEquals(3, $result);
    }

    public function testCalcPlus()
    {
        $calc = new DefaultCalc();

        $parsed = ['+' => ['1', '2']];

        $result = $calc->calc($parsed);

        $this->assertEquals(3, $result);
    }

    public function testCalcMinus()
    {
        $calc = new DefaultCalc();

        $parsed = ['-' => ['1', '2']];

        $result = $calc->calc($parsed);

        $this->assertEquals(-1, $result);
    }

    public function testCalcMultiply()
    {
        $calc = new DefaultCalc();

        $parsed = ['*' => ['2', '3']];

        $result = $calc->calc($parsed);

        $this->assertEquals(6, $result);
    }

    public function testCalcDivision()
    {
        $calc = new DefaultCalc();

        $parsed = [':' => ['4', '2']];

        $result = $calc->calc($parsed);

        $this->assertEquals(2, $result);
    }

    public function testCalcDevisionByZero()
    {
        $calc = new DefaultCalc();

        $parsed = [':' => ['4', '0']];

        $result = $calc->calc($parsed);

        $this->assertEquals(null, $result);

        $this->assertContains('Division by zero', $calc->getErrors());
    }

    public function testExecute()
    {
        $calc = new DefaultCalc();

        $result = $calc->execute('1 + 2 * 3 + 4 : 2');

        $this->assertEquals([], $result['errors']);

        $this->assertEquals(9, $result['result']);
    }
}