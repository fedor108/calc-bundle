<?php

namespace App\Command;

use Fedor108\CalcBundle\Controller\CalcController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;



class CalcCommand extends Command
{
    protected static $defaultName = 'app:calc';

    protected function configure()
    {
        $this
            ->setDescription('Console calc.')
            ->setHelp('This command allows you to use calc in console')
            ->addArgument('mathString')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mathString = $input->getArgument('mathString');

        $result = (new CalcController())->execute($mathString);
        $output->writeln(print_r($result, true));
    }
}